package pl.cm.library;

import pl.cm.library.Book;

import java.util.Scanner;

/**
 * Main class for the application.
 * @author natalia
 */
public class Main {

    /**
     * Application starting method.
     *
     * @param args Command line application starting arguments.
     */
    public static void main(String[] args) {
        /* I will use scanner for reading input. */
        Scanner inputScanner = new Scanner(System.in);

        boolean running = true;

        while (running) {


            int command = inputScanner.nextInt(); //dodawanie
            inputScanner.skip("\n");
            switch (command) {
                case 1: {
                    //addNewBook(inputScanner);
                    System.out.println("Author");
                    String author = inputScanner.nextLine();
                    System.out.println("Title");
                    String title = inputScanner.nextLine();
                    System.out.println("Year");
                    int year = inputScanner.nextInt();
                    System.out.println("index");
                    int index = inputScanner.nextInt();
                    System.out.println(author + " " + title + " " + year);
                    break;
                }
                case 0: {
                    running = false;
                    break;
                }
                default: {
                    printUsage();
                }
            }
        }
    }

    private static void printUsage() {
        System.out.println("Valid commands are:\n" +
                "1 - add,\n" +
                "2 - print,\n" +
                "0 - quit.\n");
    }
}