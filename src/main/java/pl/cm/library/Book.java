package pl.cm.library;

/**
 * Book representation.
 * @author natalia
 */
public class Book {
    /**
     * Book author.
     */
    private String author;
    /**
     * Book title.
     */
    private String title;

    /**
     * Book year of publication.
     */
    private int year;

    /**
     *
     * @param author New book author.
     * @param title New book title.
     * @param year New book year of publication.
     */

    public Book(String author, String title, int year) {
        this.author = author;
        this.title = title;
        this.year = year;
    }
    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }



}